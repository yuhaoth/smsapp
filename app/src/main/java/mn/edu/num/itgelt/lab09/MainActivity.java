package mn.edu.num.itgelt.lab09;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.provider.Telephony;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText txtMobile;
    private EditText txtMessage;
    private Button btnSms;
    TelephonyManager tm;
    PhoneStateListener phoneStateListener;
    final Date currentTime = Calendar.getInstance().getTime();

    private BroadcastReceiver powerConnectedReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
               Log.d("Itgelt", "Цэнэглэгч залгасан.");
                String deviceInfo = "Цэнэглэгч залгасан. Цаг: " + currentTime + "\n";
                try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE|Context.MODE_APPEND)) {
                    fos.write(deviceInfo.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, "Цэнэглэгч залгав.", Toast.LENGTH_SHORT).show();
        }
    };

    private BroadcastReceiver powerDisconnectedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                Log.d("Itgelt", "Цэнэглэгч салгасан.");
                String deviceInfo = "Цэнэглэгч салгасан. Цаг: " + currentTime + "\n";
                try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE|Context.MODE_APPEND)) {
                    fos.write(deviceInfo.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, "Цэнэглэгч салгав.", Toast.LENGTH_SHORT).show();
        }
    };

    private BroadcastReceiver timeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("Itgelt", "Time changed");
            String deviceInfo = "Time Changed. Цаг: " + currentTime + "\n";
            try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE|Context.MODE_APPEND)) {
                fos.write(deviceInfo.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(context, "Time Changed.", Toast.LENGTH_SHORT).show();
        }
    };

    private BroadcastReceiver timeTickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().compareTo(Intent.ACTION_TIME_TICK) %5 == 0) {
                Log.d("Itgelt", "Time Tick changed");
                String deviceInfo = "Time Tick Changed. Цаг: " + currentTime + "\n";
                try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                    fos.write(deviceInfo.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, "Time Changed.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private BroadcastReceiver batteryChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            Log.d("Itgelt", "Battery changed: " + level + "%");
                String deviceInfo = "Утасны цэнэг өөрчлөгдлөө : " +level+"%"+ " Цаг: " + currentTime + "\n";
                try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE|Context.MODE_APPEND)) {
                    fos.write(deviceInfo.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(context, String.valueOf(level) + "%", Toast.LENGTH_SHORT).show();
            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String phoneNumber) {
                super.onCallStateChanged(state, phoneNumber);
                if(state == 1) {
                    String callState = "Call state: Ringing at: " + currentTime + "\n";
                    try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                        fos.write(callState.getBytes());
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, "Дуудлага ирж байна", Toast.LENGTH_SHORT).show();
                }
                if(state == 2) {
                    String callState = "Call state: Offhook at: " + currentTime + "\n";
                    try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                        fos.write(callState.getBytes());
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, "Offhook болов", Toast.LENGTH_SHORT).show();
                }
                if(state == 0) {
                    String callState = "Call state: Idle at: " + currentTime + "\n";
                    try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                        fos.write(callState.getBytes());
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, "Idle болов", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCallForwardingIndicatorChanged(boolean cfi) {
                super.onCallForwardingIndicatorChanged(cfi);
                String callForward = "Call Forwarding Indicator Changed ! Value: "+cfi+" Time: " + currentTime + "\n";
                try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                    fos.write(callForward.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(MainActivity.this, "Call Forwarding Indicator Changed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCellInfoChanged(List<CellInfo> cellInfo) {
                super.onCellInfoChanged(cellInfo);
                try {
                    String cell = "Cell Info Changed ! Value: " + cellInfo.toString() + " Time: " + currentTime + "\n";
                    try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                        fos.write(cell.getBytes());
                        fos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(MainActivity.this, "Cell Info Changed", Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onCellLocationChanged(CellLocation location) {
                super.onCellLocationChanged(location);
                String cellLocation = "Cell Location Changed ! Value: "+location.toString()+" Time: " + currentTime + "\n";
                try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                    fos.write(cellLocation.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(MainActivity.this, "Cell Location Changed", Toast.LENGTH_SHORT).show();
            }

//            @Override
//            public void onDataActivity(int direction) {
//                super.onDataActivity(direction);
//                String data = "Data activity Changed ! Value: "+direction+" Time: " + currentTime + "\n";
//                try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
//                    fos.write(data.getBytes());
//                    fos.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                Toast.makeText(MainActivity.this, "Data activity Changed", Toast.LENGTH_SHORT).show();
//            }

            @Override
            public void onServiceStateChanged(ServiceState serviceState) {
                super.onServiceStateChanged(serviceState);
                String service = "Service State Changed ! Value: "+serviceState.toString()+" Time: " + currentTime + "\n";
                try (FileOutputStream fos = MainActivity.this.openFileOutput("phoneState.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                    fos.write(service.getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(MainActivity.this, "Service State Changed", Toast.LENGTH_SHORT).show();
            }
        };

        final Date currentTime = Calendar.getInstance().getTime();
        txtMobile = (EditText) findViewById(R.id.mblTxt);
        txtMessage = (EditText) findViewById(R.id.msgTxt);
        btnSms = (Button) findViewById(R.id.btnSend);

        this.registerReceiver(this.powerConnectedReceiver, new IntentFilter(Intent.ACTION_POWER_CONNECTED));
        this.registerReceiver(this.powerDisconnectedReceiver, new IntentFilter(Intent.ACTION_POWER_DISCONNECTED));
//        this.registerReceiver(this.batteryChangedReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        this.registerReceiver(this.timeChangedReceiver, new IntentFilter(Intent.ACTION_TIME_CHANGED));
        this.registerReceiver(this.timeTickReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));

        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SmsManager smgr = SmsManager.getDefault();
                    smgr.sendTextMessage(txtMobile.getText().toString(), null, txtMessage.getText().toString(), null, null);
                    Toast.makeText(MainActivity.this, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "SMS Failed to Send, Please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
        new PhoneStateListener();
        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if(tm != null) {
            tm.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_DATA_ACTIVITY
            | PhoneStateListener.LISTEN_SERVICE_STATE | PhoneStateListener.LISTEN_CELL_INFO | PhoneStateListener.LISTEN_CELL_LOCATION |
                    PhoneStateListener.LISTEN_CALL_FORWARDING_INDICATOR);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        String SIM = "Sim Serial: " + tm.getSimSerialNumber() + "\n" +
                "Country ISO: " + tm.getSimCountryIso() + "\n" +
                "Tel Number: " + tm.getLine1Number() + "\n" +
                "Operator Code: " + tm.getSimOperator() + "\n" +
                "Operator Name: " + tm.getSimOperatorName() + "\n";

        String filename = "SIMInfo.txt";
        try (FileOutputStream fos = MainActivity.this.openFileOutput(filename, Context.MODE_PRIVATE)) {
            fos.write(SIM.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void batteryLevel() {
        BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int rawlevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int level = -1;
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / scale;
                }
                System.out.println("Battery Level Remaining: " + level + "%");
                Toast.makeText(MainActivity.this, "Battery changed", Toast.LENGTH_LONG ).show();

            }
        };
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryLevelReceiver, batteryLevelFilter);
    }
}