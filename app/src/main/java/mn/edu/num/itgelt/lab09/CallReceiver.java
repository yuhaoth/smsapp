package mn.ed
final Date currentTime = Calendar.getInstance().getTime();
        u.num.itgelt.lab09;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.style.TtsSpan;
import android.util.Log;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class CallReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
            String callState = "TimeChanged ! :" + currentTime + "\n";
            try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                fos.write(callState.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(context, "TimeChanged", Toast.LENGTH_SHORT).show();
        }

        if(intent.getAction().compareTo(Intent.ACTION_TIME_TICK) %5 == 0) {
            String callState = "TimeChanged ! (use time tick):" + currentTime + "\n";
            try (FileOutputStream fos = context.openFileOutput("deviceInfo.txt", Context.MODE_PRIVATE | Context.MODE_APPEND)) {
                fos.write(callState.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(context, "TimeChanged: (use time tick)", Toast.LENGTH_SHORT).show();
        }
    }
}
